package monoid

import cats.instances.option._
import cats.instances.int._
import cats.instances.string._
import cats.syntax.semigroup._
import cats.instances.map._
import cats.instances.tuple._

import SuperAdder._

object monoids {
    add1(List(1, 2, 3))                           //> res0: Int = 6

    add3(List(Some(1), None, Some(2), None, Some(3)))
                                                  //> res1: Option[Int] = Some(6)

    // add(List(Some(1), Some(2), Some(3)))

    val order1 = Order(1.2, 6.46)                 //> order1  : monoid.Order = Order(1.2,6.46)
    val order2 = Order(7.4, 9.23)                 //> order2  : monoid.Order = Order(7.4,9.23)
  
    add3(List(order1, order2))                    //> res2: monoid.Order = Order(8.6,15.690000000000001)
    
    "Scala" |+| " with " |+| "Cats"               //> res3: String = Scala with Cats
    
    Option(1) |+| Option(2)                       //> res4: Option[Int] = Some(3)

    val map1 = Map("a" -> 1, "b" -> 2)            //> map1  : scala.collection.immutable.Map[String,Int] = Map(a -> 1, b -> 2)
    val map2 = Map("b" -> 3, "d" -> 4)            //> map2  : scala.collection.immutable.Map[String,Int] = Map(b -> 3, d -> 4)
    
    map1 |+| map2                                 //> res5: Map[String,Int] = Map(b -> 5, d -> 4, a -> 1)
    
    val tuple1 = ("hello", 123)                   //> tuple1  : (String, Int) = (hello,123)
    val tuple2 = ("world", 321)                   //> tuple2  : (String, Int) = (world,321)
 
    tuple1 |+| tuple2                             //> res6: (String, Int) = (helloworld,444)

    addAll(List(1, 2, 3))                         //> res7: Int = 6
    addAll(List(None, Some(1), Some(2)))          //> res8: Option[Int] = Some(3)
}