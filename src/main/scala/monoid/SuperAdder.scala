package monoid

import cats.Monoid
import cats.instances.int._
import cats.syntax.semigroup._

object SuperAdder {
  
  def add1(items: List[Int]): Int =
    items.foldLeft(0)(_ + _)

  def add2(items: List[Int]): Int =
    items.foldLeft(Monoid[Int].empty)(_ |+| _)

  def add3[A](items: List[A])(implicit monoid: Monoid[A]): A =
    items.foldLeft(monoid.empty)(_ |+| _)

  def add4[A: Monoid](items: List[A]): A =
    items.foldLeft(Monoid[A].empty)(_ |+| _)

  implicit val monoid: Monoid[Order] = new Monoid[Order] {
    def combine(o1: Order, o2: Order) =
      Order(
        o1.totalCost + o2.totalCost,
        o1.quantity + o2.quantity
      )
  
    def empty = Order(0, 0)
  }

  def addAll[A](values: List[A]) (implicit monoid: Monoid[A]): A =
    values.foldRight(monoid.empty)(_ |+| _)

}