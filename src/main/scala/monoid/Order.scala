package monoid

case class Order(totalCost: Double, quantity: Double)