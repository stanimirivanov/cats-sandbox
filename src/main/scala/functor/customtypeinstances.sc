package functor

import cats.Functor

object customtypeinstances {
  
	implicit val optionFunctor: Functor[Option] =
	  new Functor[Option] {
	    def map[A, B](value: Option[A])(func: A => B): Option[B] =
	      value.map(func)
	  }

	import scala.concurrent.{Future, ExecutionContext}

	implicit def futureFunctor
	    (implicit ec: ExecutionContext): Functor[Future] =
	  new Functor[Future] {
	    def map[A, B](value: Future[A])(func: A => B): Future[B] =
	      value.map(func)
	  }

}