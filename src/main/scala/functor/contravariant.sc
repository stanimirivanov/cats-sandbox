package functor

object contravariant {
  
  import Printable._
  import PrintableInstances._
  
	format("hello")                           //> res0: String = "hello"

	format(true)                              //> res1: String = yes
	
	format(Box("hello world"))                //> res2: String = "hello world"
	
	format(Box(123))                          //> res3: String = 123

}