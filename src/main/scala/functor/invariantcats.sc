package functor

object invariantcats {

  import cats.Monoid
	import cats.instances.string._
	import cats.syntax.invariant._
	import cats.syntax.semigroup._
	
	implicit val symbolMonoid: Monoid[Symbol] =
	  Monoid[String].imap(Symbol.apply)(_.name)
                                                  //> symbolMonoid  : cats.Monoid[Symbol] = cats.Invariant$$anon$11$$anon$9@5ae9a8
                                                  //| 29
	
	Monoid[Symbol].empty                      //> res0: Symbol = '
	
	'a |+| 'few |+| 'words                    //> res1: Symbol = 'afewwords

}