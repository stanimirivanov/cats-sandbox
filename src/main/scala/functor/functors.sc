package functor

import scala.concurrent.{Future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

import cats.instances.function._
import cats.syntax.functor._

import scala.util.Random

object functors {

	// List functor
	List(1, 2, 3).map(n => n + 1)             //> res0: List[Int] = List(2, 3, 4)

	List(1, 2, 3).
	  map(n => n + 1).
	  map(n => n * 2).
	  map(n => n + "!")                       //> res1: List[String] = List(4!, 6!, 8!)


	// Future functor
	val future: Future[String] =
	  Future(123).
	    map(n => n + 1).
	    map(n => n * 2).
	    map(n => n + "!")                     //> future  : scala.concurrent.Future[String] = Future(<not completed>)
	
	Await.result(future, 1.second)            //> res2: String = 248!

	val future1 = {
	  val r = new Random(0L)
	  val x = Future(r.nextInt)
	
	  for {
	    a <- x
	    b <- x
	  } yield (a, b)
	}                                         //> future1  : scala.concurrent.Future[(Int, Int)] = Future(<not completed>)
	
	val future2 = {
	  val r = new Random(0L)
	
	  for {
	    a <- Future(r.nextInt)
	    b <- Future(r.nextInt)
	  } yield (a, b)
	}                                         //> future2  : scala.concurrent.Future[(Int, Int)] = Future(<not completed>)
	
	val result1 = Await.result(future1, 1.second)
                                                  //> result1  : (Int, Int) = (-1155484576,-1155484576)
	val result2 = Await.result(future2, 1.second)
                                                  //> result2  : (Int, Int) = (-1155484576,-723955400)
	// Function1 functor
	val func1: Int => Double =
	  (x: Int) => x.toDouble                  //> func1  : Int => Double = functor.functors$$$Lambda$42/909295153@5abca1e0
	
	val func2: Double => Double =
	  (y: Double) => y * 2                    //> func2  : Double => Double = functor.functors$$$Lambda$43/329645619@3108bc
	
	(func1 map func2)(1)     // composition using map
                                                  //> res3: Double = 2.0
	(func1 andThen func2)(1) // composition using andThen
                                                  //> res4: Double = 2.0
	func2(func1(1))          // composition written out by hand
                                                  //> res5: Double = 2.0

	val func =
  	((x: Int) => x.toDouble).
	    map(x => x + 1).
	    map(x => x * 2).
	    map(x => x + "!")                     //> func  : Int => String = scala.Function1$$Lambda$44/1316864772@56aac163
	
	func(123)                                 //> res6: String = 248.0!
	
}