package functor

object contravariantcats {

	import cats.Contravariant._
	import cats.Show
	import cats.instances.string._
	
	val showString = Show[String]
	val showSymbol = Contravariant[Show].contramap(showString)((sym: Symbol) => s"'${sym.name}")
	
	showSymbol.show('izanami)

	import cats.syntax.contravariant._
	
	showString.contramap[Symbol](_.name).show('izanagi)

}