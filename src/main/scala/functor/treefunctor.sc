package functor

object treefunctor {
  
  // invariance problem
  //Branch(Leaf(10), Leaf(20)).map(_ * 2)

	import cats.syntax.functor._
  
  Tree.leaf(100).map(_ * 2)                       //> res0: functor.Tree[Int] = Leaf(200)

  Tree.branch(Tree.leaf(10), Tree.leaf(20)).map(_ * 2)
                                                  //> res1: functor.Tree[Int] = Branch(Leaf(20),Leaf(40))

}