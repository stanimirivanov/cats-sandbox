package functor

import cats.Functor

object catsfunctors {

	// List functor
	import cats.instances.list._

	val list1 = List(1, 2, 3)                 //> list1  : List[Int] = List(1, 2, 3)
	val list2 = Functor[List].map(list1)(_ * 2)
                                                  //> list2  : List[Int] = List(2, 4, 6)

	// Option functor
	import cats.instances.option._

	val option1 = Option(123)                 //> option1  : Option[Int] = Some(123)
	val option2 = Functor[Option].map(option1)(_.toString)
                                                  //> option2  : Option[String] = Some(123)
	// Lifted function
	val func = (x: Int) => x + 1              //> func  : Int => Int = functor.catsfunctors$$$Lambda$13/2136344592@69d9c55

	val liftedFunc = Functor[Option].lift(func)
                                                  //> liftedFunc  : Option[Int] => Option[Int] = cats.Functor$$Lambda$14/209115659
                                                  //| 6@337d0578

	liftedFunc(Option(1))                     //> res0: Option[Int] = Some(2)

	// Function1 functor
	import cats.instances.function._
	import cats.syntax.functor._

	val func1 = (a: Int) => a + 1             //> func1  : Int => Int = functor.catsfunctors$$$Lambda$15/1508395126@61a485d2
	val func2 = (a: Int) => a * 2             //> func2  : Int => Int = functor.catsfunctors$$$Lambda$16/972765878@6276ae34
	val func3 = (a: Int) => a + "!"           //> func3  : Int => String = functor.catsfunctors$$$Lambda$17/2034688500@3c09711
                                                  //| b
	val func4 = func1.map(func2).map(func3)   //> func4  : Int => String = scala.Function1$$Lambda$18/2015601401@4cc0edeb
	
	func4(123)                                //> res1: String = 248!
	
	// Mapping over functor parametarized with Int
	def doMath[F[_]](start: F[Int]) (implicit functor: Functor[F]): F[Int] =
	  start.map(n => (n + 1) * 2)             //> doMath: [F[_]](start: F[Int])(implicit functor: cats.Functor[F])F[Int]
	
	import cats.instances.option._
	import cats.instances.list._
	
	doMath(Option(20))                        //> res2: Option[Int] = Some(42)
	doMath(List(1, 2, 3))                     //> res3: List[Int] = List(4, 6, 8)

}