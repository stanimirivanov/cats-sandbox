package functor

object invariant {
  
  import Codec._
  import CodecInstances._
  
	encode(123.4)                             //> res0: String = 123.4

	decode[Double]("123.4")                   //> res1: Double = 123.4
	
	encode(Box(123.4))                        //> res2: String = 123.4

	decode[Box[Double]]("123.4")              //> res3: functor.Box[Double] = Box(123.4)
}