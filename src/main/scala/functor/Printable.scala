package functor

trait Printable[A] {
  self =>
  
  def format(value: A): String

  def contramap[B](func: B => A): Printable[B] = {
    new Printable[B] {
      override def format(value: B) = 
        self.format(func(value))
    }
  }
}

object Printable {
  def format[A](value: A)(implicit p: Printable[A]): String =
    p.format(value)
}

object PrintableInstances {
  implicit val stringPrintable =
    new Printable[String] {
      def format(value: String): String =
        "\"" + value + "\""
    }

  implicit val booleanPrintable =
    new Printable[Boolean] {
      def format(value: Boolean): String =
        if (value) "yes" else "no"
    }

  implicit val intPrintable =
    new Printable[Int] {
      def format(value: Int): String =
        value.toString
    }

  implicit def boxPrintable[A](implicit p: Printable[A]) = 
    p.contramap[Box[A]](_.value)

}