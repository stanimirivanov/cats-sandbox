package monad

import cats.Monad

object catsmonads {

	// Option monad instance
	import cats.instances.option._

	val opt1 = Monad[Option].pure(3)          //> opt1  : Option[Int] = Some(3)

	val opt2 = Monad[Option].flatMap(opt1)(a => Some(a + 2))
                                                  //> opt2  : Option[Int] = Some(5)

	val opt3 = Monad[Option].map(opt2)(a => 100 * a)
                                                  //> opt3  : Option[Int] = Some(500)
	// List monad instance
  import cats.instances.list._

	val list1 = Monad[List].pure(3)           //> list1  : List[Int] = List(3)

	val list2 = Monad[List].flatMap(List(1, 2, 3))(a => List(a, a * 10))
                                                  //> list2  : List[Int] = List(1, 10, 2, 20, 3, 30)

	val list3 = Monad[List].map(list2)(a => a + 123)
                                                  //> list3  : List[Int] = List(124, 133, 125, 143, 126, 153)

	// Vector monad instance
	import cats.instances.vector._

	Monad[Vector].flatMap(Vector(1, 2, 3))(a => Vector(a, a * 10))
                                                  //> res0: Vector[Int] = Vector(1, 10, 2, 20, 3, 30)

	// Future monad instance
	import cats.instances.future._
	import scala.concurrent._
	import scala.concurrent.duration._
	import scala.concurrent.ExecutionContext.Implicits.global
	
	val fm = Monad[Future]                    //> fm  : cats.Monad[scala.concurrent.Future] = cats.instances.FutureInstances$$
                                                  //| anon$1@18be83e4
	val future = fm.flatMap(fm.pure(1))(x => fm.pure(x + 2))
                                                  //> future  : scala.concurrent.Future[Int] = Future(<not completed>)

	Await.result(future, 1.second)            //> res1: Int = 3

	// Instances
	import cats.syntax.applicative._

	1.pure[Option]                            //> res2: Option[Int] = Some(1)
	1.pure[List]                              //> res3: List[Int] = List(1)

	// Implicit use
	import cats.syntax.functor._
	import cats.syntax.flatMap._

	def sumSquare[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
	  a.flatMap(x => b.map(y => x * x + y * y))
                                                  //> sumSquare: [F[_]](a: F[Int], b: F[Int])(implicit evidence$2: cats.Monad[F])
                                                  //| F[Int]

	sumSquare(Option(3), Option(4))           //> res4: Option[Int] = Some(25)

	sumSquare(List(1, 2, 3), List(4, 5))      //> res5: List[Int] = List(17, 26, 20, 29, 25, 34)

	def sumSquare2[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
	  for {
	    x <- a
	    y <- b
	  } yield x * x + y * y                   //> sumSquare2: [F[_]](a: F[Int], b: F[Int])(implicit evidence$3: cats.Monad[F]
                                                  //| )F[Int]
	
	sumSquare2(Option(33), Option(44))        //> res6: Option[Int] = Some(3025)

	sumSquare2(List(11, 22, 33), List(44, 55))//> res7: List[Int] = List(2057, 3146, 2420, 3509, 3025, 4114)


}