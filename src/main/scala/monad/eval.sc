package monad

import cats.Eval

object eval {
  
	val now = Eval.now(math.random + 1000)
	val later = Eval.later(math.random + 2000)
	val always = Eval.always(math.random + 3000)

	now.value
	later.value
	always.value

	val x = Eval.now {
	  println("Computing X")
	  math.random
	}

	x.value // first access
	x.value // second access

	val y = Eval.always {
	  println("Computing Y")
	  math.random
	}
	
	y.value // first access
	y.value // second access

	val z = Eval.later {
	  println("Computing Z")
	  math.random
	}
	
	z.value // first access
	z.value // second access

}