package monad

import cats.data.Reader

object reader {

  val catName: Reader[Cat, String] =
	  Reader(cat => cat.name)                 //> catName  : cats.data.Reader[monad.Cat,String] = Kleisli(monad.reader$$$Lambd
                                                  //| a$3/193064360@482f8f11)

	catName.run(Cat("Garfield", "lasagne"))   //> res0: cats.Id[String] = Garfield

	// composing readers
	val greetKitty: Reader[Cat, String] =
	  catName.map(name => s"Hello ${name}")   //> greetKitty  : cats.data.Reader[monad.Cat,String] = Kleisli(cats.data.Kleisli
                                                  //| $$Lambda$11/346861221@46d56d67)
	
	greetKitty.run(Cat("Heathcliff", "junk food"))
                                                  //> res1: cats.Id[String] = Hello Heathcliff

	val feedKitty: Reader[Cat, String] =
	  Reader(cat => s"Have a nice bowl of ${cat.favoriteFood}")
                                                  //> feedKitty  : cats.data.Reader[monad.Cat,String] = Kleisli(monad.reader$$$Lam
                                                  //| bda$13/1757676444@ae45eb6)

	val greetAndFeed: Reader[Cat, String] =
	  for {
	    greet <- greetKitty
	    feed  <- feedKitty
	  } yield s"$greet. $feed."               //> greetAndFeed  : cats.data.Reader[monad.Cat,String] = Kleisli(cats.data.Kleis
                                                  //| li$$Lambda$15/1870647526@47c62251)

	greetAndFeed(Cat("Garfield", "lasagne"))  //> res2: cats.Id[String] = Hello Garfield. Have a nice bowl of lasagne.
	
	greetAndFeed(Cat("Heathcliff", "junk food"))
                                                  //> res3: cats.Id[String] = Hello Heathcliff. Have a nice bowl of junk food.
	// Db reader
	import DbReader._
	
	val users = Map(
	  1 -> "dade",
	  2 -> "kate",
	  3 -> "margo"
	)                                         //> users  : scala.collection.immutable.Map[Int,String] = Map(1 -> dade, 2 -> ka
                                                  //| te, 3 -> margo)

	val passwords = Map(
	  "dade"  -> "zerocool",
	  "kate"  -> "acidburn",
	  "margo" -> "secret"
	)                                         //> passwords  : scala.collection.immutable.Map[String,String] = Map(dade -> zer
                                                  //| ocool, kate -> acidburn, margo -> secret)

	val db = Db(users, passwords)             //> db  : monad.Db = Db(Map(1 -> dade, 2 -> kate, 3 -> margo),Map(dade -> zeroco
                                                  //| ol, kate -> acidburn, margo -> secret))

	checkLogin(1, "zerocool").run(db)         //> res4: cats.Id[Boolean] = true
	checkLogin(4, "davinci").run(db)          //> res5: cats.Id[Boolean] = false

}