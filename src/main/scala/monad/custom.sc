package monad

object custom {
	import Tree._
  import cats.syntax.functor._
	import cats.syntax.flatMap._
	
	branch(leaf(100), leaf(200)).
	  flatMap(x => branch(leaf(x - 1), leaf(x + 1)))
                                                  //> res0: monad.Tree[Int] = Branch(Branch(Leaf(99),Leaf(101)),Branch(Leaf(199),L
                                                  //| eaf(201)))

	for {
	  a <- branch(leaf(100), leaf(200))
	  b <- branch(leaf(a - 10), leaf(a + 10))
	  c <- branch(leaf(b - 1), leaf(b + 1))
	} yield c                                 //> res1: monad.Tree[Int] = Branch(Branch(Branch(Leaf(89),Leaf(91)),Branch(Leaf(
                                                  //| 109),Leaf(111))),Branch(Branch(Leaf(189),Leaf(191)),Branch(Leaf(209),Leaf(21
                                                  //| 1))))

}