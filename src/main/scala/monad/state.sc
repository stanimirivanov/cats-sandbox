package monad

import cats.data.State

object state {
  
	val a = State[Int, String] { state =>
	  (state, s"The state is $state")
	}                                         //> a  : cats.data.State[Int,String] = cats.data.IndexedStateT@2b05039f

	// Get the state and the result:
	val (state, result) = a.run(10).value     //> state  : Int = 10
                                                  //| result  : String = The state is 10
	
	// Get the state, ignore the result:
	val state2 = a.runS(10).value             //> state2  : Int = 10
	
	// Get the result, ignore the state:
	val result2 = a.runA(10).value            //> result2  : String = The state is 10

	val step1 = State[Int, String] { num =>
	  val ans = num + 1
	  (ans, s"Result of step1: $ans")
	}                                         //> step1  : cats.data.State[Int,String] = cats.data.IndexedStateT@cb5822
	
	val step2 = State[Int, String] { num =>
	  val ans = num * 2
	  (ans, s"Result of step2: $ans")
	}                                         //> step2  : cats.data.State[Int,String] = cats.data.IndexedStateT@2b98378d
	
	val both = for {
	  a <- step1
	  b <- step2
	} yield (a, b)                            //> both  : cats.data.IndexedStateT[cats.Eval,Int,Int,(String, String)] = cats.d
                                                  //| ata.IndexedStateT@4c70fda8
	
	val (state3, result3) = both.run(20).value//> state3  : Int = 42
                                                  //| result3  : (String, String) = (Result of step1: 21,Result of step2: 42)
	val getDemo = State.get[Int]              //> getDemo  : cats.data.State[Int,Int] = cats.data.IndexedStateT@604ed9f0
	getDemo.run(10).value                     //> res0: (Int, Int) = (10,10)
	
	val setDemo = State.set[Int](30)          //> setDemo  : cats.data.State[Int,Unit] = cats.data.IndexedStateT@685cb137
	setDemo.run(10).value                     //> res1: (Int, Unit) = (30,())
	
	val pureDemo = State.pure[Int, String]("Result")
                                                  //> pureDemo  : cats.data.State[Int,String] = cats.data.IndexedStateT@7cd62f43
	pureDemo.run(10).value                    //> res2: (Int, String) = (10,Result)
	
	val inspectDemo = State.inspect[Int, String](_ + "!")
                                                  //> inspectDemo  : cats.data.State[Int,String] = cats.data.IndexedStateT@6093dd9
                                                  //| 5
	inspectDemo.run(10).value                 //> res3: (Int, String) = (10,10!)
	
	val modifyDemo = State.modify[Int](_ + 1) //> modifyDemo  : cats.data.State[Int,Unit] = cats.data.IndexedStateT@57baeedf
	modifyDemo.run(10).value                  //> res4: (Int, Unit) = (11,())

	// with for comprehention
	import State._

	val program: State[Int, (Int, Int, Int)] = for {
	  a <- get[Int]
	  _ <- set[Int](a + 1)
	  b <- get[Int]
	  _ <- modify[Int](_ + 1)
	  c <- inspect[Int, Int](_ * 1000)
	} yield (a, b, c)                         //> program  : cats.data.State[Int,(Int, Int, Int)] = cats.data.IndexedStateT@5
                                                  //| 3b32d7
	
	val (state4, result5) = program.run(1).value
                                                  //> state4  : Int = 3
                                                  //| result5  : (Int, Int, Int) = (1,2,3000)

	// Clculator
	import Calculator._

	evalOne("42").runA(Nil).value             //> res5: Int = 42

	val program2 = for {
	  _   <- evalOne("1")
	  _   <- evalOne("2")
	  ans <- evalOne("+")
	} yield ans                               //> program2  : cats.data.IndexedStateT[cats.Eval,List[Int],List[Int],Int] = ca
                                                  //| ts.data.IndexedStateT@589838eb
	
	program2.runA(Nil).value                  //> res6: Int = 3

	val program3 = evalAll(List("1", "2", "+", "3", "*"))
                                                  //> program3  : monad.Calculator.CalcState[Int] = cats.data.IndexedStateT@6aded
                                                  //| e5

	program3.runA(Nil).value                  //> res7: Int = 9

	val program4 = for {
	  _   <- evalAll(List("1", "2", "+"))
	  _   <- evalAll(List("3", "4", "+"))
	  ans <- evalOne("*")
	} yield ans                               //> program4  : cats.data.IndexedStateT[cats.Eval,List[Int],List[Int],Int] = ca
                                                  //| ts.data.IndexedStateT@49993335
	
	program4.runA(Nil).value                  //> res8: Int = 21

	evalInput("1 2 + 3 4 + *")                //> res9: Int = 21
}