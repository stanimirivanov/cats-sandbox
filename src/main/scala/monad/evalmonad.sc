package monad

import cats.Eval

object evalmonad {

	val greeting = Eval
	  .always { println("Step 1"); "Hello" }
	  .map { str => println("Step 2"); s"$str world" }
                                                  //> greeting  : cats.Eval[String] = cats.Eval$$anon$8@cac736f

	greeting.value                            //> Step 1
                                                  //| Step 2
                                                  //| res0: String = Hello world

	val ans = for {
	  a <- Eval.now { println("Calculating A"); 40 }
	  b <- Eval.always { println("Calculating B"); 2 }
	} yield {
	  println("Adding A and B")
	  a + b
	}                                         //> Calculating A
                                                  //| ans  : cats.Eval[Int] = cats.Eval$$anon$8@7225790e

	ans.value // first access                 //> Calculating B
                                                  //| Adding A and B
                                                  //| res1: Int = 42
	ans.value // second access                //> Calculating B
                                                  //| Adding A and B
                                                  //| res2: Int = 42

  val saying = Eval
	  .always { println("Step 1"); "The cat" }
	  .map { str => println("Step 2"); s"$str sat on" }
	  .memoize
	  .map { str => println("Step 3"); s"$str the mat" }
                                                  //> saying  : cats.Eval[String] = cats.Eval$$anon$8@12843fce
	
	saying.value // first access              //> Step 1
                                                  //| Step 2
                                                  //| Step 3
                                                  //| res3: String = The cat sat on the mat
	saying.value // second access             //> Step 3
                                                  //| res4: String = The cat sat on the mat

	// defer
	def factorial(n: BigInt): Eval[BigInt] =
	  if(n == 1) {
	    Eval.now(n)
	  } else {
	    Eval.defer(factorial(n - 1).map(_ * n))
	  }                                       //> factorial: (n: BigInt)cats.Eval[BigInt]

	factorial(50000).value                    //> res5: BigInt = 3347320509597144836915476094071486477912773223810454807730100
                                                  //| 3219901680221443656416973812310719169308798480438190208299893616384743066693
                                                  //| 7426305728453637840383257562821233599872682440782359723560408538544413733837
                                                  //| 5356856553637116832740516607615516592140615607546129420179056747966549862924
                                                  //| 2220022541553510718159801615476451810616674970217996537474972541139338191638
                                                  //| 8235006303076442568748572713946510819098749096434862685892298078700310310089
                                                  //| 6286115455397991161294065232739697149721103126114286073379350968783735581183
                                                  //| 0609551728906603833592532851635961730885279811957399495299450306354442478492
                                                  //| 6410289900695596348835299005576765509291754759207880448076225624151651304590
                                                  //| 4631806851740676636001232955645406572422517547342818312102919571559378742364
                                                  //| 1117194513838593038006413132976312508980623953869845352836267459097392518734
                                                  //| 7791738698054874418218564843850349196433374384607147670018127809768669571553
                                                  //| 722962855502892722067813
                                                  //| Output exceeds cutoff limit.
	
	// safe fold
	def foldRightEval[A, B](as: List[A], acc: Eval[B])
	    (fn: (A, Eval[B]) => Eval[B]): Eval[B] =
	  as match {
	    case head :: tail =>
	      Eval.defer(fn(head, foldRightEval(tail, acc)(fn)))
	    case Nil =>
	      acc
	  }                                       //> foldRightEval: [A, B](as: List[A], acc: cats.Eval[B])(fn: (A, cats.Eval[B])
                                                  //|  => cats.Eval[B])cats.Eval[B]

	def foldRight[A, B](as: List[A], acc: B)(fn: (A, B) => B): B =
	  foldRightEval(as, Eval.now(acc)) { (a, b) =>
	    b.map(fn(a, _))
	  }.value                                 //> foldRight: [A, B](as: List[A], acc: B)(fn: (A, B) => B)B
	
	foldRight((1 to 100000).toList, 0L)(_ + _)//> res6: Long = 5000050000
		
}