package monad

object catserrorhandling {
  
	import cats.MonadError
	import cats.instances.either._
	
	type ErrorOr[A] = Either[String, A]
	
	val monadError = MonadError[ErrorOr, String]
                                                  //> monadError  : cats.MonadError[monad.catserrorhandling.ErrorOr,String] = cats
                                                  //| .instances.EitherInstances$$anon$1@48533e64

	val success = monadError.pure(42)         //> success  : monad.catserrorhandling.ErrorOr[Int] = Right(42)

	val failure = monadError.raiseError("Badness")
                                                  //> failure  : monad.catserrorhandling.ErrorOr[Nothing] = Left(Badness)

	monadError.handleError(failure) {
	  case "Badness" =>
	    monadError.pure("It's ok")
	
	  case other =>
	    monadError.raiseError("It's not ok")
	}                                         //> res0: monad.catserrorhandling.ErrorOr[monad.catserrorhandling.ErrorOr[String
                                                  //| ]] = Right(Right(It's ok))

	monadError.ensure(success)("Number too low!")(_ > 1000)
                                                  //> res1: monad.catserrorhandling.ErrorOr[Int] = Left(Number too low!)

	import cats.syntax.applicative._
	import cats.syntax.applicativeError._
	import cats.syntax.monadError._

	val success2 = 42.pure[ErrorOr]           //> success2  : monad.catserrorhandling.ErrorOr[Int] = Right(42)

	val failure2 = "Badness".raiseError[ErrorOr, Int]
                                                  //> failure2  : monad.catserrorhandling.ErrorOr[Int] = Left(Badness)

	success2.ensure("Number to low!")(_ > 1000)
                                                  //> res2: monad.catserrorhandling.ErrorOr[Int] = Left(Number to low!)

	import scala.util.Try
	import cats.instances.try_._
	
	val exn: Throwable = new RuntimeException("It's all gone wrong")
                                                  //> exn  : Throwable = java.lang.RuntimeException: It's all gone wrong
	
	exn.raiseError[Try, Int]                  //> res3: scala.util.Try[Int] = Failure(java.lang.RuntimeException: It's all gon
                                                  //| e wrong)

}