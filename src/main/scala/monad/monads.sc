package monad

object monads {

	// Option monad
	def parseInt(str: String): Option[Int] =
	  scala.util.Try(str.toInt).toOption

	def divide(a: Int, b: Int): Option[Int] =
	  if(b == 0) None else Some(a / b)

	def stringDivideBy(aStr: String, bStr: String): Option[Int] =
	  parseInt(aStr).flatMap { aNum =>
	    parseInt(bStr).flatMap { bNum =>
	      divide(aNum, bNum)
	    }
	  }

	stringDivideBy("6", "2")

	stringDivideBy("6", "0")

	stringDivideBy("6", "foo")

	stringDivideBy("bar", "2")

	// with for comprehention
	def stringDivideBy2(aStr: String, bStr: String): Option[Int] =
	  for {
	    aNum <- parseInt(aStr)
	    bNum <- parseInt(bStr)
	    ans  <- divide(aNum, bNum)
	  } yield ans

	stringDivideBy2("8", "2")

	stringDivideBy2("8", "0")

	stringDivideBy2("8", "foo")

	stringDivideBy2("bar", "4")

	// Future monad
	import scala.concurrent.Future
	import scala.concurrent.ExecutionContext.Implicits.global

	def doSomethingLongRunning: Future[Int] = Future(123)
	def doSomethingElseLongRunning: Future[Int] = Future(456)
	
	def doSomethingVeryLongRunning: Future[Int] =
	  for {
	    result1 <- doSomethingLongRunning
	    result2 <- doSomethingElseLongRunning
	  } yield result1 + result2

	def doSomethingVeryLongRunning2: Future[Int] =
	  doSomethingLongRunning.flatMap { result1 =>
	    doSomethingElseLongRunning.map { result2 =>
	      result1 + result2
	    }
	  }

}