package monad

final case class Cat(name: String, favoriteFood: String)