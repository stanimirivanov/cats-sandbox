package monad

object errorhandling {

	import cats.syntax.either._

  for {
	  a <- 1.asRight[String]
	  b <- 0.asRight[String]
	  c <- if(b == 0) "DIV0".asLeft[Int]
	       else (a / b).asRight[String]
	} yield c * 100                           //> res0: scala.util.Either[String,Int] = Left(DIV0)


	type LoginResult = Either[LoginError, User]

	// Choose error-handling behaviour based on type:
	def handleError(error: LoginError): Unit =
	  error match {
	    case UserNotFound(u) =>
	      println(s"User not found: $u")
	
	    case PasswordIncorrect(u) =>
	      println(s"Password incorrect: $u")
	
	    case UnexpectedError =>
	      println(s"Unexpected error")
	  }                                       //> handleError: (error: monad.LoginError)Unit

	val result1: LoginResult = User("dave", "passw0rd").asRight
                                                  //> result1  : monad.errorhandling.LoginResult = Right(User(dave,passw0rd))

	val result2: LoginResult = UserNotFound("dave").asLeft
                                                  //> result2  : monad.errorhandling.LoginResult = Left(UserNotFound(dave))
	
	result1.fold(handleError, println)        //> User(dave,passw0rd)

	result2.fold(handleError, println)        //> User not found: dave

}