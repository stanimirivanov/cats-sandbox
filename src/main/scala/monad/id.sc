package monad

import cats.Id
import cats.Monad

object id {
  
	import cats.syntax.functor._
	import cats.syntax.flatMap._
	
	def sumSquare[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
	  for {
	    x <- a
	    y <- b
	  } yield x * x + y * y                   //> sumSquare: [F[_]](a: F[Int], b: F[Int])(implicit evidence$2: cats.Monad[F])F
                                                  //| [Int]

	sumSquare(3 : Id[Int], 4 : Id[Int])       //> res0: cats.Id[Int] = 25

	val a = Monad[Id].pure(3)                 //> a  : cats.Id[Int] = 3
	val b = Monad[Id].flatMap(a)(_ + 1)       //> b  : cats.Id[Int] = 4

	for {
	  x <- a
	  y <- b
	} yield x + y                             //> res1: cats.Id[Int] = 7


}