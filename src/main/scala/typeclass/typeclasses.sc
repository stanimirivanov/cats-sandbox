package typeclass

import JsonWriterInstances._
import JsonSyntax._

import PrintableInstances._
import PrintableSyntax._

import cats.Eq
import cats.instances.int._

import cats.instances.option._
import java.util.Date

import cats.Show
import cats.instances.int._
import cats.instances.string._

import cats.syntax.show._
import ShowInstances._

object typeclasses {

  // Printable
  // method 1: Interface Object
  val person1 = Json.toJson(Person("Izanami", "izanami@mail.com"))(personWriter)
                                                  //> person1  : typeclass.Json = JsObject(Map(name -> JsString(Izanami), email ->
                                                  //|  JsString(izanami@mail.com)))
  println("Person as Json from Interface Object: " + person1)
                                                  //> Person as Json from Interface Object: JsObject(Map(name -> JsString(Izanami)
                                                  //| , email -> JsString(izanami@mail.com)))

  // method 2: Interface Syntax
  val person2 = Person("Izanagi", "izanagi@mail.com").toJson
                                                  //> person2  : typeclass.Json = JsObject(Map(name -> JsString(Izanagi), email ->
                                                  //|  JsString(izanagi@mail.com)))
  println("Person as Json from Interface Syntax: " + person2)
                                                  //> Person as Json from Interface Syntax: JsObject(Map(name -> JsString(Izanagi)
                                                  //| , email -> JsString(izanagi@mail.com)))

  val str = Json.toJson(Option("A string"));      //> str  : typeclass.Json = JsString(A string)
  println("Option of String: " + str)             //> Option of String: JsString(A string)

  val cat = Cat("Garfield", 38, "ginger and black")
                                                  //> cat  : typeclass.Cat = Cat(Garfield,38,ginger and black)
  Printable.print(cat)                            //> Garfield is a 38 year-old ginger and black cat.

  Cat("Garfield", 38, "ginger and black").print   //> Garfield is a 38 year-old ginger and black cat.
  
  
  // Eq
	val eqInt = Eq[Int]                       //> eqInt  : cats.kernel.Eq[Int] = cats.kernel.instances.IntOrder@27efef64
	eqInt.eqv(123, 123)                       //> res0: Boolean = true
	eqInt.eqv(123, 234)                       //> res1: Boolean = false
	
	import cats.syntax.eq._
	
	123 === 123                               //> res2: Boolean = true
	123 =!= 234                               //> res3: Boolean = true
	
	// Some(1) === None
	(Some(1): Option[Int]) === (None: Option[Int])
                                                  //> res4: Boolean = false
	Option(1) === Option.empty[Int]           //> res5: Boolean = false
	
	import cats.syntax.option._
	
	1.some === none[Int]                      //> res6: Boolean = false
	1.some =!= none[Int]                      //> res7: Boolean = true
	
	import EqInstances._
	
	val x = new Date()                        //> x  : java.util.Date = Sun Dec 03 10:04:46 CET 2017
	val y = new Date()                        //> y  : java.util.Date = Sun Dec 03 10:04:46 CET 2017
	
	println(x === x)                          //> true
	println(x === y)                          //> false
	
	val cat1 = Cat("Garfield", 38, "orange and black")
                                                  //> cat1  : typeclass.Cat = Cat(Garfield,38,orange and black)
	val cat2 = Cat("Heathcliff", 32, "orange and black")
                                                  //> cat2  : typeclass.Cat = Cat(Heathcliff,32,orange and black)
	
	cat1 === cat2                             //> res8: Boolean = false
	cat1 =!= cat2                             //> res9: Boolean = true
	
	val optionCat1 = Option(cat1)             //> optionCat1  : Option[typeclass.Cat] = Some(Cat(Garfield,38,orange and black
                                                  //| ))
	val optionCat2 = Option.empty[Cat]        //> optionCat2  : Option[typeclass.Cat] = None
	
	optionCat1 === optionCat2                 //> res10: Boolean = false
	optionCat1 =!= optionCat2                 //> res11: Boolean = true
	
	// Show
	val showInt: Show[Int] = Show.apply[Int]  //> showInt  : cats.Show[Int] = cats.Show$$anon$2@ff5b51f
	val showString: Show[String] = Show.apply[String]
                                                  //> showString  : cats.Show[String] = cats.Show$$anon$2@25bbe1b6
	
	val intAsString: String = showInt.show(123)
                                                  //> intAsString  : String = 123
	val stringAsString: String = showString.show("abc")
                                                  //> stringAsString  : String = abc
	
	println(intAsString)                      //> 123
	println(stringAsString)                   //> abc
	
	println(123.show)                         //> 123
	println("abc".show)                       //> abc
	
	val cat3 = Cat("Garfield", 38, "ginger and black")
                                                  //> cat3  : typeclass.Cat = Cat(Garfield,38,ginger and black)
	println(cat3.show)                        //> Garfield is a 38 year-old ginger and black cat.
}