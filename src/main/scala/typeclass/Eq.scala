package typeclass

import cats.Eq
import cats.syntax.eq._
import cats.instances.int._
import cats.instances.long._
import cats.instances.string._

import java.util.Date

object EqInstances {
  implicit val dateEq: Eq[Date] = Eq.instance[Date] { (date1, date2) =>
    date1.getTime === date2.getTime
  }

  implicit val catEq = Eq.instance[Cat] { (c1, c2) =>
    c1.name === c2.name &&
    c1.age === c2.age &&
    c1.color === c2.color
  }
}
